<div id="top"></div>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <h3 align="center">Enhance Pages with Backend Stuff (Express JS)</h3>

  <p align="center">
    Get to know Node JS, HTTP Server, Express JS, and RestFul API
    <br />
    <br />
  </p>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
        <li><a href="#feature">Features</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

A repository for completing Binar Fullstack Web Development Bootcamp - Chapter 5.

<p align="right">(<a href="#top">back to top</a>)</p>

### Built With

The following lists show my development stack:

- [Bootstrap v5.0](https://getbootstrap.com/)
- [Bootstrap Icons](https://icons.getbootstrap.com/)
- [Sass](https://sass-lang.com/)
- [Vanilla Javascript](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
- [NodeJS including NPM](https://nodejs.org/en/)
- [ExpressJS](https://expressjs.com/)
- [EJS as View Engine](https://ejs.co/)
- [Uuid](https://www.npmjs.com/package/uuid)
- [Nodemon](https://www.npmjs.com/package/nodemon)
- [Morgan](https://www.npmjs.com/package/morgan)

Tools:
[Git](https://git-scm.com/)
[Neovim](https://neovim.io/)

<p align="right">(<a href="#top">back to top</a>)</p>

### Feature

Kind of additional page for previous challenge, from this chapter I learnt about backend stuff like:

- `Node JS` => How to setup project properly and manage node script with proper name and the reason we use it
- `Core Module` => I never knew about `http-server` core module before, but this chapter I realized that the module is the backbone of Express, and also learning much about `fs` module
- `Express JS` => Practical implementation and better understanding of middleware, router, and error handler
- `RestFul API` => Now I know about how RestFul implementation (GET, POST, PUT, PATCH, DELETE) is so important for backend since it represent the intend of user request, just on point.

**NOTE: You can use dedicated endpoints for CRUD of users object in path** `/api/v1/users`

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- GETTING STARTED -->

## Getting Started

### Prerequisites

If you want to edit the code, you need to have `nodejs` and `NPM`. Note: Sass files are not included here.

- Install all dependencies by this command if you already get node and npm installed in your system.
  ```sh
  npm install
  ```


## Usage

- Start the server by this command, it will run `node app.js`
  ```sh
  npm run start
  ```
- Open `localhost:3000` in your browser

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- LICENSE -->

## License

Distributed under the MIT License.

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- CONTACT -->

## Contact

Sedana Yoga - [@cok_yoga](https://twitter.com/Cok_Yoga)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
