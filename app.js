const express = require('express')
const morgan = require('morgan')
const fs = require('fs')
const db = require('./db/users.json')
const dotenv = require('dotenv')
const routerApi = require('./routes/version/apiV1')
const { v4: uuidv4 } = require('uuid')
const { notFound, errorHandler } = require('./middleware/errorMiddleware.js')

dotenv.config()

const PORT = 3000
const users = db
let logProps = {
  isLogged: false,
  loggedAs: '',
}

// Decare express instance with app variable
const app = express()

// Set static path
app.use(express.static('public'))

// Use parser middleware (BUILT-IN LEVEL MIDDLEWARE)
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use('/api/v1/', routerApi)

// Use view engine and third party middleware
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'))
}
app.set('view engine', 'ejs')

// The API
// HOME ENDPOINT
app.get('/', (req, res) => {
  res.render('home', logProps)
})

// GAME ENDPOINT
app.get('/rps-game', (req, res) => {
  res.render('rps')
})

// SIGNUP ENDPOINT
app.get('/signup', (req, res) => {
  res.render('sign-in-up', { signType: 'up' })
})

app.post('/signup', (req, res) => {
  if (
    !req.body.username ||
    req.body.username === '' ||
    !req.body.email ||
    req.body.email === '' ||
    !req.body.password ||
    req.body.password === ''
  )
    res.status(400).json({ message: 'Please enter all user data' })
  const { username, email, password, confirmPassword } = req.body
  if (password !== confirmPassword) {
    res.send("Password doesn't match")
  }
  const id = uuidv4()

  users.push({ id, username, email, password })
  console.log(users)
  logProps.isLogged = true
  logProps.loggedAs = username

  fs.writeFileSync('./db/users.json', JSON.stringify(users))

  res.redirect('/')
})

// SIGNIN ENDPOINT
app.get('/signin', (req, res) => {
  res.render('sign-in-up', { signType: 'in' })
})

app.post('/signin', (req, res) => {
  const { username: userName, password } = req.body

  const loggedUser = users.find((user) => user.username === userName)

  if (!loggedUser || loggedUser.password !== password) {
    res.send('Unable to login, wrong username or password')
  }

  logProps.loggedAs = loggedUser.username
  logProps.isLogged = true

  res.redirect('/')
})

// LOGOUT ENDPOINT
app.get('/logout', (req, res) => {
  logProps = { isLogged: false, loggedAs: '' }
  res.redirect('/')
})

// Error 500 example
app.get('/iniError', (req, res) => {
  errorTest
})

// Error handler
app.use(notFound)
app.use(errorHandler)

// START SERVER
app.listen(PORT, () => {
  console.log(`Server has started in ${PORT}`)
})
